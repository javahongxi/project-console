# Project Console

## sentinel-dashboard
启动dashboard后再启动应用

## rocketmq-console
启动nameserver后再启动console

## dubbo-admin
1. git clone https://github.com/apache/dubbo-admin
1. 执行`mvn clean package -Dmaven.test.skip`
1. 启动zookeeper
1. 启动dubbo-admin-server
1. 浏览器打开`localhost:8080`
